<?php namespace Test\Blank\Components;

use Test\Blank\Models\Product;
use Cms\Classes\ComponentBase;

class Product extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Product Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getProduct($id)
    {        
        return Product::where('id', $id)->first();
    }

    public function getProductComments($id)
    {        
        return Product::where('id', $id)->first()->comments();
    }
}
