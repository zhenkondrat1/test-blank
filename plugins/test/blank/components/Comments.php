<?php 

namespace Test\Blank\Components;
//use Test\Blank\Models\Product;
use Test\Blank\Models\Comment;
use Cms\Classes\ComponentBase;
use Input;
use Redirect;
use Flash;

class Comments extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Comments Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getComments($id)
    {        
        return Comment::where('product_id', $id)
                        ->orderby('id','desc')
                        ->get();
    }


    public function onFileUpload()
    {
        $file = new \System\Models\File;
        $file->is_public = true;
        $file->data = Input::file('file-upload');
        $file->save();


        $model = new Comment;
        //$model->attach = $file->getPath(); 
        $model->attach = Input::file('file-upload');//()->add($file); 
       // The above line assumes you have proper attachOne or attachMany relationships defined on your model - in this case, I have named the relationship simply as "file"
        $model->name = Input::get('name');
        $model->email = Input::get('email');
        $model->content = Input::get('text');
        $model->product_id = Input::get('id');
        $model->file_path =  $file->getPath();//url('/') .
        $model->save();
        Flash::success('Відгук збережено!');
        return Redirect::back();
    }
}
